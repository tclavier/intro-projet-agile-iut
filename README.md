# Projets agile de l'IUT de Lille

* Support pour les étudiants de S3 (FI et FC) : [slides S3](https://gitlab.com/tclavier/intro-projet-agile-iut/builds/artifacts/master/file/n3fifc.pdf?job=build_pdf)
* Support pour les étudiants de S4 (FI et FC) : [slides S4](https://gitlab.com/tclavier/intro-projet-agile-iut/builds/artifacts/master/file/n4fifc.pdf?job=build_pdf)

