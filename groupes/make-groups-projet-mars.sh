#!/bin/bash 
set -x

GROUP_SIZE=8

#S2-Annee-1-FI.csv  S3-FC.csv  S3-FI.csv

# Build S3 list
tail -n +2 S4-FI.csv | head -n -5 | grep -v AAPEL | cut -f 4 -d ';' > students.lst

# Take FC
tail -n +2 S4-FC.csv | head -n -5 | grep -v AAPEL | cut -f 4 -d ';' >> students.lst

NB_STUDENTS=$(wc -l students.lst | cut -f 1 -d ' ')
RESTE=$(($NB_STUDENTS % $GROUP_SIZE))
NB_GROUPS=$(($NB_STUDENTS / $GROUP_SIZE))
[ $RESTE -eq 0 ] || NB_GROUPS=$(($NB_GROUPS + 1))
#NB_GROUPS=16
echo "nombre de groupes : $NB_GROUPS"

> groups.lst
IFS_OLD=$IFS
IFS='|'
i=0
for student in $(cat students.lst | tr '\n' '|')
do
  grp=$(($i%$NB_GROUPS))
  echo "$student;$grp" >> groups.lst
  i=$(($i+1))
done
IFS=$IFS_OLD

LAST_GROUP="NO"
echo "<!doctype html>" > groups.html
echo '<html lang="en">' >> groups.html
echo '  <head>' >> groups.html
echo '    <meta charset="utf-8">' >> groups.html
echo '    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">' >> groups.html
echo '    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>' >> groups.html
echo '  </head>' >> groups.html
echo '  <body>' >> groups.html
echo '    <div class="card-group">' >> groups.html
IFS_OLD=$IFS
IFS='|'
for line in $(cat groups.lst | sort -k 2 -t ';' -n | tr '\n' '|')
do
  STUDENT=$(echo $line | cut -f 1 -d ";")
  GROUP=$(echo $line | cut -f 2 -d ";")

  if [ $LAST_GROUP != $GROUP ]
  then
    if [ $GROUP -gt 0 ]
    then
      echo "  </div></div>" >> groups.html
    fi
    GROUP_LINE=$(($GROUP % 6))
    if [ $GROUP_LINE -eq 0 ]
    then
      echo '    </div>' >> groups.html
      echo '    <div class="card-group">' >> groups.html
    fi
    echo "<div class='card' style='width: 18rem;'>" >> groups.html
    echo "  <div class='card-body'>" >> groups.html
    echo "    <h2 class='card-title'>Groupe $GROUP</h5>" >> groups.html
    echo "    <ul>" >> groups.html
  fi
  echo   "      <li>$STUDENT</li>" >> groups.html
  LAST_GROUP=$GROUP
done
IFS=$IFS_OLD

#cat groups.lst | sort -k 2 -t ';' -n | tr ';' '\t'
